import lxml.etree as ET
from dateutil.parser import parse as parse_datetime, ParserError


def filter_meetings(meeting_date):
    # read input xml file
    xml_tree = ET.parse("assets/meetings.xml")

    # filter suitable meetings
    root = xml_tree.getroot()
    for child in root.getchildren():
        current_datetime = parse_datetime(child.xpath("MeetingDatetime")[0].text)
        if current_datetime.date() != meeting_date:
            root.remove(child)

    # load xls stylesheet
    stylesheet = ET.parse("assets/meetings.xsl")

    # build and write html result
    html_tree = ET.XSLT(stylesheet)(xml_tree)
    with open("assets/result.html", "w") as output_file:
        output_file.write(ET.tostring(html_tree).decode())
        print("Result successfully stored to result.html file")


if __name__ == "__main__":
    meeting_date_raw = input("Enter meeting date: ")
    try:
        meeting_date = parse_datetime(meeting_date_raw).date()
    except ParserError:
        print("Invalid date")
        exit(1)

    filter_meetings(meeting_date)
