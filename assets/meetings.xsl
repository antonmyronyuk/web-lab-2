<?xml version = "1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes"/>
<xsl:template match="/">
<html>
  <body>
  	<xsl:apply-templates/>
  </body>
</html>
</xsl:template>
<xsl:template match="MeetingsInfo">
	<table border="2" width="100%">
		<tr bgcolor="LIGHTBLUE">
			<td>Name</td>
			<td>Location</td>
			<td>Attendees</td>
			<td>Datetime</td>
		</tr>
		<xsl:for-each select="Meeting">
			<tr bgcolor="LIGHTYELLOW">
				<td><i><xsl:value-of select="MeetingName"/></i></td>
				<td><xsl:value-of select="MeetingLocation"/></td>
				<td><xsl:value-of select="MeetingAttendees"/></td>
				<td bgcolor="LIGHTGREEN"><xsl:value-of select="MeetingDatetime"/></td>
			</tr>
		</xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>
